import { ADD_USER, SET_IS_AUTHENTICATED, SET_IS_LOADING_AUTHENTICATED, LOGOUT_USER } from '../actionTypes'

const initialState = {
  isLoadingAuthenticated: false,
  user: null,
  isAuthenticated: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_USER: {
      
      const { user } = action
      
      return {
        ...state,
        user,
        isAuthenticated: true
      };
    }
    case SET_IS_AUTHENTICATED: {
      
      const { isAuthenticated } = action
      
      return {
        ...state,
        isAuthenticated
      }
    }
    case SET_IS_LOADING_AUTHENTICATED: {
      
      const { isLoadingAuthenticated } = action
      
      return {
        ...state,
        isLoadingAuthenticated
      }
    }    
    case LOGOUT_USER: {
      
      return {
        ...state,
        user: null,
        isAuthenticated: false
      }
    }
    default:
      return state;
  }
}
