import { ADD_USER, SET_IS_AUTHENTICATED, SET_IS_LOADING_AUTHENTICATED, LOGOUT_USER } from './actionTypes'
import { Auth } from 'aws-amplify'
import isEmpty from 'lodash.isempty'

export const addUser = user => ({
  type: ADD_USER,
  user
});

export const setIsAuthenticated = isAuthenticated => ({
  type: SET_IS_AUTHENTICATED,
  isAuthenticated
})

export const setIsLoadingAuthenticated = isLoadingAuthenticated => ({
  type: SET_IS_LOADING_AUTHENTICATED,
  isLoadingAuthenticated
})

export const logoutUser = () => ({
  type: LOGOUT_USER
})

export const login = (username, password) => {

  return async (dispatch) => {

    try {

      const user = await Auth.signIn(username, password)

      return dispatch(addUser(user))
    } catch (err) {

      console.log(`login() err:`, err)
    }
  }
}

export const checkAuthentication = () => {;

  return async (dispatch) => {

    try {

      dispatch(setIsLoadingAuthenticated(true))

      const user = await Auth.currentAuthenticatedUser()

      if (isEmpty(user)) {

        dispatch(setIsAuthenticated(false))

        return dispatch(setIsLoadingAuthenticated(false))
      }

      dispatch(setIsAuthenticated(true))

      dispatch(addUser(user))

      return dispatch(setIsLoadingAuthenticated(false))      
    } catch (err) {
      
      dispatch(setIsAuthenticated(false))

      return dispatch(setIsLoadingAuthenticated(false))     
    }
  }
}

export const logout = () => {

  return async (dispatch) => {

    try {

      await Auth.signOut()  

      return dispatch(logoutUser())
    } catch (err) {

      console.log(`logout() err:`, err)
    }
  }
}