import React from 'react'
import { Router, Route } from 'react-router-dom'
import AuthContainer from './components/Auth/AuthContainer'
import { createBrowserHistory } from 'history'
import MenuContainer from './components/Menu/MenuContainer'
import DashboardContainer from './components/Dashboard/DashboardContainer'
import AuthenticatedRouteContainer from './components/Route/AuthenticatedRouteContainer'
import UnAuthenticatedRouteContainer from './components/Route/UnAuthenticatedRouteContainer'
import { configureAuthentication } from './utils/auth'

configureAuthentication()

const history = createBrowserHistory()

const Routes = () => (
  <Router history={history}>
    <MenuContainer />
    <main className="pt-12">
      <Route exact path="/" render={() => <AuthenticatedRouteContainer exact path="/" component={DashboardContainer} />} />
      <Route exact path="/dashboard" render={() => <AuthenticatedRouteContainer exact path="/dashboard" component={DashboardContainer} />} />    
      <Route path="/auth" render={() => <UnAuthenticatedRouteContainer path="/auth" component={AuthContainer} />} />
    </main>    
  </Router>
)

export default Routes
