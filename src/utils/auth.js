import Amplify, { Auth } from 'aws-amplify'
import store from '../store'
import { addUser } from '../store/actions'
import isEmpty from 'lodash.isempty'

export const configureAuthentication = () => {

  Amplify.configure({
    Auth: {
      region: process.env.REACT_APP_AWS_COGNITO_REGION,
      userPoolId: process.env.REACT_APP_AWS_COGNITO_POOL_ID,
      userPoolWebClientId: process.env.REACT_APP_AWS_COGNITO_APP_CLIENT_ID,
      authenticationFlowType: 'USER_PASSWORD_AUTH'
    }      
  })
}