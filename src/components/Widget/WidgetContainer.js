import { connect } from 'react-redux'
import { addUser } from '../../store/actions'
import Widget from './Widget'

const mapStateToProps = (state) => ({
  user: state.auth.user,
  isAuthenticated: state.auth.isAuthenticated
})

const mapDispatchToProps = (dispatch) => ({
  addUser: user => {
    dispatch(addUser(user))
  }
})

const WidgetContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Widget)

export default WidgetContainer