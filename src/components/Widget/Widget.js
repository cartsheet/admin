import React from 'react'
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
// import { dark } from 'react-syntax-highlighter/dist/esm/styles/prism'

const code = `
<body>
  ...
  <script src=".../sup.js"></script>
</body>
`

const Widget = () => {

  const handleSubmit = () => {

    console.log(`handleSubmit()`)
  }

  return (
    <div>
      <h2 className="text-center pb-4">Widget</h2>
      <SyntaxHighlighter language="html">
        {code}
      </SyntaxHighlighter>     
    </div>    
  )
}

export default Widget