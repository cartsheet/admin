import React, { Component } from 'react'
import { Menu, Icon } from 'antd'
import { Link } from 'react-router-dom'

const { SubMenu }  = Menu;

class MenuComponent extends Component {

  state = {
    current: 'mail',
  }

  handleClick = e => {

    this.setState({
      current: e.key,
    });
  }

  render () {

    const { logout, login, isAuthenticated, user } = this.props

    const MenuOptions = isAuthenticated ?  <SubMenu title={<span className="submenu-title-wrapper"><Icon type="setting" />Settings</span>}>
      {/* <Menu.Item key="setting:1">Billing</Menu.Item> */}
        <Menu.Item
          key="setting:2"
          onClick={logout}>
            Logout
        </Menu.Item>
      </SubMenu>
      :
      <Menu.Item onClick={login} key="setting:1">Login</Menu.Item>

    return (
      <Menu
        onClick={this.handleClick}
        selectedKeys={[this.state.current]}
        className="bg-white"
        style={{ zIndex: 10, position: `fixed`, width: `100%`}}
        mode="horizontal">
        <Menu.Item key="logo">
          <h2><Link to="/">CartSheet</Link></h2>
        </Menu.Item>
        {MenuOptions}
      </Menu>
    );
  }
}

export default MenuComponent
