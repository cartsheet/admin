import { connect } from 'react-redux'
import { addUser } from '../../store/actions'
import Credentials from './Credentials'

const mapStateToProps = (state) => ({
  user: state.auth.user,
  isAuthenticated: state.auth.isAuthenticated
})

const mapDispatchToProps = (dispatch) => ({
  addUser: user => {
    dispatch(addUser(user))
  }
})

const CredentialsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Credentials)

export default CredentialsContainer