import React from 'react'
import {
  Form,
  Input,
  Button
} from 'antd'

const Credentials = () => {

  const handleSubmit = () => {

    console.log(`handleSubmit()`)
  }

  return (
    <Form onSubmit={handleSubmit}>
      <h2 className="text-center pb-4">Credentials</h2>
      <div className="flex items-center mb-4">
        <label className="flex-1">Stripe API Key</label>
        <Input className="flex-2" />
      </div>
      <div className="flex items-center mb-4">
        <label className="flex-1">Google Sheet ID</label>
        <Input className="flex-2" />
      </div>  
      <div className="flex justify-end">
        <Button type="primary" htmlType="submit">Save</Button>    
      </div>        
    </Form>    
  )
}

export default Credentials