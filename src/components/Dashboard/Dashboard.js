import React from 'react'
import CredentialsContainer from '../Credentials/CredentialsContainer'
import WidgetContainer from '../Widget/WidgetContainer'

const Dashboard = () => {

  return (
    <main className="m-4">
      <div className="shadow-lg w-300 md:w-400 p-4">
        <CredentialsContainer />
      </div>
      <div className="shadow-lg w-300 md:w-400 p-4">
        <WidgetContainer />
      </div>      
    </main>
  )  
}

export default Dashboard