import { connect } from 'react-redux'
import { addUser } from '../../store/actions'
import Dashboard from './Dashboard'

const mapStateToProps = (state) => ({
  user: state.auth.user,
  isAuthenticated: state.auth.isAuthenticated
})

const mapDispatchToProps = (dispatch) => ({
  addUser: user => {
    dispatch(addUser(user))
  }
})

const DashboardContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard)

export default DashboardContainer