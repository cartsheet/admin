import { connect } from 'react-redux'
import { addUser, setIsAuthenticated, checkAuthentication } from '../../store/actions'
import UnAuthenticatedRoute from './UnAuthenticatedRoute'

const mapStateToProps = (state) => ({
  user: state.auth.user,
  isAuthenticated: state.auth.isAuthenticated,
  isLoadingAuthenticated: state.auth.isLoadingAuthenticated,
})

const mapDispatchToProps = (dispatch) => ({
  addUser: user => {
    dispatch(addUser(user))
  },
  setIsAuthenticated: isAuthenticated => {
    dispatch(setIsAuthenticated(isAuthenticated))
  },
  checkAuthentication: () => {
    dispatch(checkAuthentication())
  }
})

const UnAuthenticatedRouteContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UnAuthenticatedRoute)

export default UnAuthenticatedRouteContainer