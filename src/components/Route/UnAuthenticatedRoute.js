import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'
import isNil from 'lodash.isnil'
import Loading from '../Loading/Loading'

class UnAuthenticatedRoute extends Component {

  async componentDidMount () {

    const { isAuthenticated, isLoadingAuthenticated, checkAuthentication } = this.props

    if (isNil(isAuthenticated) && !isLoadingAuthenticated) {

      checkAuthentication()
    }
  }

  render () {

    const { component: Component, isAuthenticated, isLoadingAuthenticated, ...rest } = this.props

    if (isLoadingAuthenticated) return <Loading />
    
    return (
      <Route {...rest} render={(props) => (
        isAuthenticated === false
          ? <Component {...props} />
          : <Redirect to='/' />
      )} />
    )}
}

export default UnAuthenticatedRoute