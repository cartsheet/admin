import React, { useState } from 'react'
import { Form, Icon, Input, Button, Checkbox } from 'antd'
import { withRouter, Link } from 'react-router-dom'

const Login = ({ login }) => {

  const [ email, setEmail ] = useState(``)
  const [ password, setPassword ] = useState(``)

  const onChangePassword = (event) => setPassword(event.target.value)
  const onChangeEmail = (event) => setEmail(event.target.value)

  const handleSubmit = async (event) => {

    event.preventDefault()

    login(email, password)
  }

  return (
    <Form onSubmit={handleSubmit} className="login-form">
      <Form.Item>
        <Input
          onChange={onChangeEmail}
          value={email}
          prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
          placeholder="Email"
        />
      </Form.Item>
      <Form.Item>
        <Input
          onChange={onChangePassword}
          value={password}
          prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
          type="password"
          placeholder="Password"
        />
      </Form.Item>
      <div className="w-full flex justify-between mb-4">
        <Link to="/auth/forgot-password" className="login-form-forgot">
          Forgot password
        </Link>
        <span>Or <Link to='/auth/sign-up'>register now!</Link></span>
      </div>
      <Button type="primary" htmlType="submit" className="login-form-button w-full mb-4">
        Log in
      </Button>
    </Form>
  )
}

export default withRouter(Login)