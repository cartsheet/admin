import React, { useState } from 'react'
import { Auth } from 'aws-amplify'
import { Form, Icon, Input, Button, Checkbox } from 'antd'
import { withRouter, Link } from 'react-router-dom'

const Login = () => {

  const [ email, setEmail ] = useState(``)
  const [ oldPassword, setOldPassword ] = useState(``)
  const [ newPassword, setNewPassword ] = useState(``)

  const handleSubmit = async (event) => {

    event.preventDefault()

    const changePasswordResponse = await Auth.changePassword(email, oldPassword, newPassword);
  
    console.log(`changePasswordResponse: `, changePasswordResponse)
  }

  return (
    <Form onSubmit={handleSubmit} className="login-form">
      <Form.Item>
        <Input
          onChange={e => setEmail(e.target.value)}
          value={email}
          prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
          placeholder="Email"
        />
      </Form.Item>
      <Form.Item>
        <Input
          onChange={e => setOldPassword(e.target.value)}
          value={oldPassword}
          prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
          type="password"
          placeholder="Old Password"
        />
      </Form.Item>
      <Form.Item>
        <Input
          onChange={e => setNewPassword(e.target.value)}
          value={newPassword}
          prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
          type="password"
          placeholder="New Password"
        />
      </Form.Item>      
      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button w-full">
          Reset Password
        </Button>
      </Form.Item>      
    </Form>
  )
}

export default withRouter(Login)