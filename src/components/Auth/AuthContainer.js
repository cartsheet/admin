import { connect } from 'react-redux'
import { login } from '../../store/actions'
import Auth from './Auth'

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
    isAuthenticated: state.auth.isAuthenticated
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: (username, password) => {
      dispatch(login(username, password))
    }
  }
}

const AuthPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Auth)

export default AuthPageContainer