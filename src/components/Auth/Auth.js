import React from 'react'
import Login from './Login'
import SignUp from './SignUp'
import CodeConfirmation from './CodeConfirmation'
import ForgotPassword from './ForgotPassword'
import { Menu, Icon } from 'antd'
import { Route, Link, withRouter } from 'react-router-dom'

const Auth = ({ location, addUser, user, login }) => {

  return (
    <main className="login-card w-full h-full flex items-center justify-center pt-16">
      <div className="shadow-lg w-300 md:w-400">
        <Menu selectedKeys={[location.pathname]} mode="horizontal">
          <Menu.Item key="/auth/login">
            <Link to="/auth/login">
              <Icon type="appstore" />
              Login
            </Link>                  
          </Menu.Item>
          <Menu.Item key="/auth/sign-up">
            <Link to="/auth/sign-up">
              <Icon type="mail" />
              Sign Up
            </Link>
          </Menu.Item>
        </Menu>
          <div className="pt-4 pr-4 pl-4">
            <Route exact path="/auth/login" render={() => <Login login={login} addUser={addUser} user={user}/>} />
            <Route exact path="/auth/sign-up" render={() => <SignUp addUser={addUser} user={user} />} />
            <Route exact path="/auth/code" render={() => <CodeConfirmation addUser={addUser} user={user} />} />
            <Route exact path="/auth/forgot-password" render={() => <ForgotPassword addUser={addUser} user={user} />} />
          </div>
      </div>
    </main>
  )  
}

export default withRouter(Auth)