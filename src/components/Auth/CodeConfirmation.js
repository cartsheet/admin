import React, { useState } from 'react'
import { Auth } from 'aws-amplify'
import { Form, Icon, Input, Button, Checkbox } from 'antd'
import { withRouter } from 'react-router-dom'

const CodeConfirmation = ({ history, user }) => {

  const [ code, changeCode ] = useState(``)
  const [ email, changeEmail ] = useState(user.username)

  const handleResend = async () => {

    changeCode(``)

    const resendResponse = await Auth.resendSignUp(email)

    console.log(`resendResponse: `, resendResponse)
  }
  
  const handleSubmit = async (event) => {

    event.preventDefault()

    try {

      const confirmResponse = await Auth.confirmSignUp(email, code)
      
      console.log(`confirmResponse: `, confirmResponse)

      if (confirmResponse !== `SUCCESS`) {
        
        throw new Error(confirmResponse)
      }

      history.push(`/`)
    } catch (error) {

      console.log(`handleSignUp() error: `, error)
    }
  }

  return (
    <Form onSubmit={handleSubmit} className="login-form">
      <Form.Item>
        <p>Check your email for a confirmation code</p>
      </Form.Item>
      <Form.Item>
        <Input
          onChange={e => changeEmail(e.target.value)}
          value={email}
          prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
          placeholder="Email"
        />
      </Form.Item>
      <Form.Item>
        <Input
          onChange={e => changeCode(e.target.value)}
          value={code}
          prefix={<Icon type="key" style={{ color: 'rgba(0,0,0,.25)' }} />}
          placeholder="Code Confirmation"
        />
      </Form.Item>
      <div className="w-full flex justify-between mb-4">
        <a onClick={handleResend} className="login-form-forgot">
          Resend code
        </a>
      </div>
      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button w-full">
          Confirm
        </Button>
      </Form.Item>
    </Form>
  )
}

export default withRouter(CodeConfirmation)